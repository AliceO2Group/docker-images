# Base script
The script `inherit_deploy.sh` takes a tag and image to inherit from as arguments. 
It then builds an image and pushes it to GitLab with the tag.
Scripts need to be executed as root for Docker. You will also need to input credentials for GitLab.
Example:
```
./inherit_deploy.sh etcd-v3.1.3 quay.io/coreos/etcd:v3.1.3
```

# Specialized scripts
The scripts `deploy_[x].sh` call `inherit_deploy.sh` with suitable arguments.

# Pulling images
Images can be pulled with the command:
```
docker pull gitlab-registry.cern.ch/aliceo2group/docker-images:[tag]
```

# Dependencies
Requires Docker to be installed and running
```
yum -y install docker
systemctl start docker
```
