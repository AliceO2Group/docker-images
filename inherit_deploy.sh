#!/bin/bash

echo "Note: this script must be run as root to connect to Docker"

# Argument for tag to use
TAG=$1
# Argument for image to inherit from
INHERIT=$2

REGISTRY="gitlab-registry.cern.ch"
IMAGE="$REGISTRY/aliceo2group/docker-images:$TAG"
DIR="/tmp/docker-image-build/"

echo "Enter GitLab credentials:"
docker login $REGISTRY

mkdir $DIR
cd $DIR
echo "FROM $INHERIT" > Dockerfile
docker build -t $IMAGE .
rm Dockerfile
cd -
rmdir $DIR

docker push $IMAGE
